import unittest
from unittest.mock import patch, mock_open
from PIL import Image
from photo_checker.main import read_files, is_different


class TestPhotoChecker(unittest.TestCase):
    @patch("os.scandir")
    def test_read_files(self, mock_scandir):
        # Set up mocked scandir to return a list of mocked "file" objects
        mock_scandir.return_value = [
            mock_file("image1.jpg"),
            mock_file("image2.png"),
            mock_file("image3.gif"),
            mock_file("file.txt"),
        ]
        self.assertEqual(
            read_files("test_dir"), ["image1.jpg", "image2.png", "image3.gif"]
        )

    @patch("PIL.Image.open")
    def test_is_different_same_image(self, mock_open):
        # Set up mocked image objects to be returned by PIL.Image.open
        mock_image1 = mock_open.return_value
        mock_image2 = mock_open.return_value

        self.assertFalse(is_different("image1.jpg", ["image1.jpg", "image2.png"]))

    # TODO: This code is not working properly
    @patch("PIL.Image.open")
    def test_is_different_diff_image(self, mock_open):
        # Set up mocked image objects to be returned by PIL.Image.open
        mock_image1 = mock_open.return_value
        mock_image1.tobytes.return_value = b"1234"
        mock_image2 = mock_open.return_value
        mock_image2.tobytes.return_value = b"1234"

        self.assertFalse(is_different(mock_image1, [mock_image1, mock_image2]))


def mock_file(name):
    """Helper function to create a mocked "file" object"""
    mock_file = mock_open()
    mock_file.name = name
    mock_file.path = name
    return mock_file


if __name__ == "__main__":
    unittest.main()
