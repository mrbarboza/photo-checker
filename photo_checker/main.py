import os
from datetime import datetime
from PIL import Image, ImageChops


def is_different(image_path: str, images: list[str]) -> bool:
    with Image.open(image_path) as img:
        for image in images:
            if image != image_path:
                compare: Image = Image.open(image)
                diff = ImageChops.difference(image1=img, image2=compare)
                if diff.getbbox() is None:
                    return True
    return False


def read_files(directory: str) -> list[str]:
    extensions: list[str] = [
        "jpeg",
        "jpg",
        "png",
        "gif",
        "bmp",
        "tiff",
        "ico",
        "webp",
        "svg",
    ]

    return [
        file.path
        for file in os.scandir(directory)
        if any(file.name.endswith(f".{ext}") for ext in extensions)
    ]


def main() -> None:
    directory: str = "../photo-checker/photo_checker/images"
    files = read_files(directory)
    for file in files:
        if is_different(file, files):
            print(f"{file.split('/')[-1]} has a duplicated photo")
        else:
            print(f"{file.split('/')[-1]} doesn't have a duplicated photo")


if __name__ == "__main__":
    main()
